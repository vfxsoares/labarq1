// --------------------------------------------------------------------
// Copyright (c) 2010 by Terasic Technologies Inc. 
// --------------------------------------------------------------------
//
// Permission:
//
//   Terasic grants permission to use and modify this code for use
//   in synthesis for all Terasic Development Boards and Altera Development 
//   Kits made by Terasic.  Other use of this code, including the selling 
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  Terasic provides no warranty regarding the use 
//   or functionality of this code.
//
// --------------------------------------------------------------------
//           
//                     Terasic Technologies Inc
//                     356 Fu-Shin E. Rd Sec. 1. JhuBei City,
//                     HsinChu County, Taiwan
//                     302
//
//                     web: http://www.terasic.com/
//                     email: support@terasic.com
//
// --------------------------------------------------------------------
//
// Major Functions:	DE2_115_Default
//
// --------------------------------------------------------------------
//
// Revision History :
// --------------------------------------------------------------------
//   Ver  :| Author              :| Mod. Date :| Changes Made:
//   V1.1 :| HdHuang             :| 05/12/10  :| Initial Revision
// --------------------------------------------------------------------

module DE2_115_Default(

	//////// CLOCK //////////
	CLOCK_50,
    CLOCK2_50,
    CLOCK3_50,
	ENETCLK_25,

	//////// KEY //////////
	KEY,

	//////// LCD //////////
	LCD_BLON,
	LCD_DATA,
	LCD_EN,
	LCD_ON,
	LCD_RS,
	LCD_RW
);

	
//=======================================================
//  PARAMETER declarations
//=======================================================


//=======================================================
//  PORT declarations
//=======================================================

//////////// CLOCK //////////
input		          		CLOCK_50;
input		          		CLOCK2_50;
input		          		CLOCK3_50;
input		          		ENETCLK_25;

//////////// LCD //////////
output		        		LCD_BLON;
inout		      [7:0]		LCD_DATA;
output		          	    LCD_EN;
output		          	    LCD_ON;
output		          	    LCD_RS;
output		        		LCD_RW;

//////////// KEY //////////
input		      [3:0]		KEY;


///////////////////////////////////////////////////////////////////
//=============================================================================
// REG/WIRE declarations
//=============================================================================


wire	CPU_CLK;
wire	CPU_RESET;

reg [31:0]	Cont;

// DEFAULT LCD       //

//wire      LCD_ON_1  ;
//wire      LCD_BLON_1;
wire [7:0]  LCD_D_1;
wire        LCD_RW_1;
wire        LCD_EN_1;
wire        LCD_RS_1;

//=============================================================================
// Structural coding
//=============================================================================
// initial //
//
assign LCD_DATA = LCD_D_1;
assign LCD_RW   = LCD_RW_1;
assign LCD_EN   = LCD_EN_1;
assign LCD_RS   = LCD_RS_1; 
assign LCD_ON   = 1'b1;
assign LCD_BLON = 1'b0; //not supported;
			

always@(posedge CLOCK_50 or negedge KEY[0])
    begin
        if(!KEY[0])
			 Cont	<=	0;
        else
			 Cont	<=	Cont+1;
    end

LCD_TEST 			u5	(	//	Host Side
							.iCLK(CLOCK_50),
							.iRST_N(DLY_RST),
							//	LCD Side
							.LCD_DATA(LCD_D_1),
							.LCD_RW(LCD_RW_1),
							.LCD_EN(LCD_EN_1),
							.LCD_RS(LCD_RS_1)	);


endmodule
