`timescale 1ns / 1ps
//------------------------------------------------
// topmulti.sv
// David_Harris@hmc.edu 9 November 2005
// Update to SystemVerilog 17 Nov 2010 DMH
// Top level system including multicycle MIPS 
// and unified memory
//------------------------------------------------

module topmulti (
  input wire reset, 
	input wire CLOCK_50,	//board clock: 50MHz
	input wire CLOCK2_50,
	input wire CLOCK3_50,
	input wire ENETCLK_25,
	output wire LCD_BLON,
	output wire LCD_DATA,
	output wire LCD_EN,
	output wire LCD_ON,
	output wire LCD_RS,
	output wire LCD_RW
	);

	clockdiv cd(
	.clr(reset),
	.clk(CLOCK_50),
	.dclk(CLOCK_25)
	);
  
  logic [31:0] writedata, adr, readdata, vadr, vdata;
  logic        memwrite;

	  
  
  // microprocessor (control & datapath)
  mips mips(CLOCK_25, reset, adr, writedata, memwrite, readdata);

  // memory 
  mem mem(CLOCK_25, memwrite, adr, writedata, vadr, readdata, vdata);

  // LCD
  DE2_115_Default DE2_115_Default(CLOCK_50, CLOCK2_50, CLOCK3_50, ENETCLK_25, LCD_BLON, LCD_DATA, LCD_EN, LCD_ON, LCD_RS, LCD_RW);

endmodule
